from loader import dp, logi
from aiogram import types

@dp.message_handler()
async def access_denied(message: types.Message):
    logi.info(f"Попытка ввода команды {message.text} без состояния")
    await message.answer("Доступ запрещен")
