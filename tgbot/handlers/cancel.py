from loader import dp, logi
from states import sign
from aiogram import types
from aiogram.dispatcher import FSMContext

@dp.message_handler(commands=["cancel"],state="*")
async def cancel(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("Сброшено")
    logi.info("Состояние сброшено через /cancel")
