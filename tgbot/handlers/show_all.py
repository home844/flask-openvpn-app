import os
from loader import dp, reqsdir, logi
from states import sign
from aiogram import types

# show only directories from '/app/reqs'
@dp.message_handler(commands=["showall"])
async def show_all(message: types.Message):
    logi.info("Введена команда /showall")
    dirs = [name_dir for name_dir in os.listdir(reqsdir) if os.path.isdir(os.path.join(reqsdir, name_dir))]
    list_dirs = "\n".join(dirs)

    await message.answer(f"Выберете один запрос:\n{list_dirs}")
    await sign.Form.req_id.set()
    logi.info("Установлено состояние")
