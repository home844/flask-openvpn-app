import os, shutil, subprocess
from loader import dp, reqsdir, logi, bot
from states import sign
from aiogram import types
from aiogram.dispatcher import FSMContext

@dp.message_handler(state=sign.Form.confirm)
async def confirm_sign(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['confirm'] = message.text
        user_input = data['confirm']
        req_id = data['req_id']

        if user_input == '/sign':
            logi.info("Введена команда /sign")
            shutil.copy2(f'{reqsdir}/{req_id}/vars', '/app/pki/vars') # copying vars to pki catalog
            subprocess.call(['sed', '-i', '8s:/app/easyrsa3:${0%/*}:', '/app/pki/vars']) # substitution string
            subprocess.call(['sed', '-i', 's/\$EASYRSA\/pki/\$PWD\/pki/', '/app/pki/vars']) # substitution string
            subprocess.call(['/app/easyrsa3/easyrsa', 'sign-req', 'client', f'{req_id}']) # signing req
            logi.info("Попытка подписания запроса")

            subprocess.call(['/bin/sh', '-c', f'/app/easyrsa3/create_cert.sh {req_id}']) # generating ovpn-file
            logi.info("Попытка генерации ovpn-файла")

            await state.finish()

            ovpnfile = open(f'/app/easyrsa3/ovpn_config/{req_id}.ovpn', 'rb')
            await bot.send_document(message.chat.id, document=ovpnfile) # sending ovpn-file
            logi.info("Попытка отправить файл")
            logi.info("Состояние завершилось")
        else:
            await state.finish()    
            await message.answer("Неверная команда, состояние сброшено")
            logi.info(f"Неверная команда {message.text}, состояние сброшено")
