import os, json
from loader import dp, reqsdir, logi
from states import sign
from aiogram import types
from aiogram.dispatcher import FSMContext

@dp.message_handler(state=sign.Form.req_id)
async def show_req_id(message: types.Message, state: FSMContext):
    logi.info(f"Введено {message.text} в состоянии req_id")
    async with state.proxy() as data:
        data['req_id'] = message.text
   
    # parsing json from /app/reqs 
    jsonfile = open(f'{reqsdir}/{message.text}/{message.text}.txt', 'r')
    string_jsonfile = jsonfile.read()
    datadict = json.loads(string_jsonfile)
    alldata = f"""
Запрос: {message.text}
Страна: {datadict['country'][0]}
Область: {datadict['any_name'][0]}
Город: {datadict['any_name'][1]}
Организация: {datadict['any_name'][2]}
Подразделение: {datadict['any_name'][3]}
Телеграм: {datadict['any_name'][4]}
Почта: {datadict['email'][0]}

Для подписания сертификата введите /sign
Для сброса состояния - /cancel
    """

    await message.answer(alldata)
    await sign.Form.next()
    logi.info("Состояние передано в confirm")
