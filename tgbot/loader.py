import os, logging
from dotenv import load_dotenv
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage

load_dotenv() # loading variables from .env

token = os.environ.get('TOKEN')
reqsdir = os.environ.get('REQSDIR')

logi = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, filename="log/tgbot.log", encoding='utf-8', format="%(filename)s: #%(levelname)-8s " "[%(asctime)s] - %(message)s")

bot = Bot(token=token)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
