from aiogram import executor
from loader import dp
from handlers import cancel, sign_req, view_one, show_all, access_denied

if __name__ == '__main__':
    executor.start_polling(dp)
