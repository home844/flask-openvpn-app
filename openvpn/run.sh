#/bin/sh

ADAPTER=eth0
NETWORK=10.10.1.0/24

# Allow UDP traffic on port 1194.
iptables -A INPUT -i "$ADAPTER" -p udp -m state --state NEW,ESTABLISHED --dport 1194 -j ACCEPT
iptables -A OUTPUT -o "$ADAPTER" -p udp -m state --state ESTABLISHED --sport 1194 -j ACCEPT

# Allow traffic on the TUN interface.
iptables -A INPUT -i tun0 -j ACCEPT
iptables -A FORWARD -i tun0 -j ACCEPT
iptables -A OUTPUT -o tun0 -j ACCEPT

# Allow forwarding traffic only from the VPN.
iptables -A FORWARD -i tun0 -o "$ADAPTER" -s "$NETWORK" -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

iptables -t nat -A POSTROUTING -s "$NETWORK" -o "$ADAPTER" -j MASQUERADE

openvpn --config /etc/openvpn/server.conf

