#!/usr/bin/env sh

#===============Variables===================
REQ_ID="$1"
CERTNAME="$REQ_ID.ovpn"
DIROVPN="/app/easyrsa3/ovpn_config" # /app/easyrsa3 - path in containers for tgbot
SERVER_HOSTNAME="hostname your server"

#===============Create certificate===================
echo "client
dev tun
proto udp
remote $SERVER_HOSTNAME 1194
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
cipher AES-256-CBC
auth SHA256
verb 4
tls-version-min 1.2
tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256:TLS-DHE-RSA-WITH-AES-128-GCM-SHA256:TLS-DHE-RSA-WITH-AES-128-CBC-SHA256" > $DIROVPN/$CERTNAME

echo '<ca>' >> $DIROVPN/$CERTNAME
cat pki/ca.crt >> $DIROVPN/$CERTNAME
echo '</ca>' >> $DIROVPN/$CERTNAME

echo '<cert>' >> $DIROVPN/$CERTNAME
cat pki/issued/$REQ_ID.crt >> $DIROVPN/$CERTNAME
echo '</cert>' >> $DIROVPN/$CERTNAME

echo '<key>' >> $DIROVPN/$CERTNAME
cat pki/private/$REQ_ID.key >> $DIROVPN/$CERTNAME
echo '</key>' >> $DIROVPN/$CERTNAME
