#!/usr/bin/env bash

#===============Initialization PKI===================
./easyrsa init-pki

#===============Create vars file===================
echo 'if [ -z "$EASYRSA_CALLER" ]; then
        echo "You appear to be sourcing an Easy-RSA *vars* file. This is" >&2
        echo "no longer necessary and is disallowed. See the section called" >&2
        echo "*How to use this file* near the top comments for more details." >&2
        return 1
fi

set_var EASYRSA 		"${0%/*}"

set_var EASYRSA_OPENSSL 	"openssl"
set_var EASYRSA_PKI             "$PWD/pki"
set_var EASYRSA_TEMP_DIR        "$EASYRSA_PKI"
set_var EASYRSA_DN              "org"

set_var EASYRSA_REQ_COUNTRY     "RU"
set_var EASYRSA_REQ_PROVINCE    "Sverdlovskaya obl."
set_var EASYRSA_REQ_CITY        "Yekaterinburg"
set_var EASYRSA_REQ_ORG         "Home"
set_var EASYRSA_REQ_EMAIL       "andislamov@gmail.com"
set_var EASYRSA_REQ_OU          "Laptop"
set_var EASYRSA_REQ_CN          "OpenVPN-PKI"

set_var EASYRSA_NO_PASS 1

set_var EASYRSA_KEY_SIZE        2048
set_var EASYRSA_ALGO            rsa
set_var EASYRSA_CURVE           secp384r1
set_var EASYRSA_CA_EXPIRE       3650
set_var EASYRSA_CERT_EXPIRE     3650
set_var EASYRSA_CRL_DAYS        180
set_var EASYRSA_RAND_SN "yes"
set_var EASYRSA_CERT_RENEW      90
set_var EASYRSA_FIX_OFFSET 	1
set_var EASYRSA_NS_SUPPORT      "no"
set_var EASYRSA_NS_COMMENT      "Easy-RSA Generated Certificate"

set_var EASYRSA_EXT_DIR         "$EASYRSA/x509-types"
set_var EASYRSA_KDC_REALM       "CHANGEME.EXAMPLE.COM"
set_var EASYRSA_SSL_CONF        "$EASYRSA_PKI/openssl-easyrsa.cnf"
set_var EASYRSA_DIGEST          "sha256"

set_var EASYRSA_BATCH           "yes"
' > pki/vars

#===============Create ca.crt===================
./easyrsa build-ca

#===============Edit CN===================
sed -i 's/OpenVPN-PKI/OpenVPN-Server/' pki/vars

#===============Create private key and request===================
./easyrsa gen-req openvpn-server

#===============Issue cert===================
./easyrsa sign-req server openvpn-server

#===============Create keys DH===================
./easyrsa gen-dh

#===============Create revoked list===================
./easyrsa gen-crl
