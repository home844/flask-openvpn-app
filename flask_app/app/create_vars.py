import os, json, shutil

pkidir = os.environ.get('PKIDIR')

def create_vars(req_id): # variable "req_id" is read_random()
    jsonfile = open(f'reqs/{req_id}/{req_id}.txt', 'r') # io.TextIOWrapper
    string_jsonfile = jsonfile.read() # converting io.TextIOWrapper to string
    datadict = json.loads(string_jsonfile) # decoding json to dict

    template = f"""if [ -z "$EASYRSA_CALLER" ]; then
        echo "You appear to be sourcing an Easy-RSA *vars* file. This is" >&2
        echo "no longer necessary and is disallowed. See the section called" >&2
        echo "*How to use this file* near the top comments for more details." >&2
        return 1
fi

    set_var EASYRSA                 "{pkidir}"

    set_var EASYRSA_OPENSSL         "openssl"
    set_var EASYRSA_PKI             "{pkidir}/pki"
    set_var EASYRSA_TEMP_DIR        "$EASYRSA_PKI"
    set_var EASYRSA_DN              "org"

    set_var EASYRSA_NO_PASS 1
    set_var EASYRSA_KEY_SIZE        2048
    set_var EASYRSA_ALGO            rsa
    set_var EASYRSA_CURVE           secp384r1
    set_var EASYRSA_CA_EXPIRE       3650
    set_var EASYRSA_CERT_EXPIRE     3650
    set_var EASYRSA_CRL_DAYS        180
    set_var EASYRSA_RAND_SN	    "no"
    set_var EASYRSA_CERT_RENEW      90
    set_var EASYRSA_FIX_OFFSET      1
    set_var EASYRSA_NS_SUPPORT      "no"
    set_var EASYRSA_NS_COMMENT      "Easy-RSA Generated Certificate"
    set_var EASYRSA_EXT_DIR         "$EASYRSA/x509-types"
    set_var EASYRSA_KDC_REALM       "CHANGEME.EXAMPLE.COM"
    set_var EASYRSA_SSL_CONF        "$EASYRSA_PKI/openssl-easyrsa.cnf"
    set_var EASYRSA_DIGEST          "sha256"
    set_var EASYRSA_BATCH           "yes"

    set_var EASYRSA_REQ_COUNTRY	"{datadict['country'][0]}"
    set_var EASYRSA_REQ_PROVINCE "{datadict['any_name'][0]}"
    set_var EASYRSA_REQ_CITY	"{datadict['any_name'][1]}"
    set_var EASYRSA_REQ_ORG	"{datadict['any_name'][2]}"
    set_var EASYRSA_REQ_OU	"{datadict['any_name'][3]}"
    set_var EASYRSA_REQ_CN      "{datadict['any_name'][4]}"
    set_var EASYRSA_REQ_EMAIL	"{datadict['email'][0]}"
    """

    vars_file = open( f'reqs/{req_id}/vars', 'w')
    vars_file.write(template)
    vars_file.close()

def copy_vars(req_id):
    shutil.copy2(f'reqs/{req_id}/vars', f'{pkidir}/pki/vars')