from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Email, Length

class ReqForm(FlaskForm):
    country = StringField(validators=[DataRequired(), Length(min=2, max=2, message="Только две буквы, ни больше, ни меньше")])
    email = StringField(validators=[Email("Некорректный e-mail")])
    any_name = StringField(validators=[DataRequired(), Length(max=32)])
    submit = SubmitField("Отправить")