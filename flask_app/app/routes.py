from modules.index import index_bp
from modules.ok import ok_bp
from modules.error_404 import error404_bp

def route(app):
	app.register_blueprint(index_bp)
	app.register_blueprint(ok_bp)
	app.register_blueprint(error404_bp)