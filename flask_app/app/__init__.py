from flask import Flask
from app.forms import ReqForm # own module
from app.routes import route # own module
from config import Config # own module

def create_flask_app():
	app = Flask(__name__)
	app.config.from_object(Config) # connecting config.py
	route(app) #connecting routes.py

	return app