import os, json
from flask import Flask, render_template, url_for, request, redirect, session

def create_dir(req_id): # variable "req_id" is read_random()
    path = f'reqs/{req_id}'
    os.mkdir(path)

def record_json(req_id): # variable "req_id" is read_random()
    data_dict=(request.form.to_dict(flat=False)) # flatten
    json_obj=json.dumps(data_dict, indent = 4) # converting to json

    req_file = open( f'reqs/{req_id}/{req_id}.txt', 'w')
    req_file.write(json_obj)
    req_file.close()