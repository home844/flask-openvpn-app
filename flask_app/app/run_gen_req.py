import os, subprocess

pkidir = os.environ.get('PKIDIR')

def run_easyrsa(req_id): # variable "req_id" is read_random()
    subprocess.Popen([f"{pkidir}/easyrsa", f"--vars={pkidir}/pki/vars", "gen-req", f"{req_id}"])