import random, os

def random_number():
	req_id = str(random.randint(10000, 99999)) # creating random number
	req_random_file = open(f'reqs/random', 'w')
	req_random_file.write(f'{req_id}')
	req_random_file.close()

def read_random():
	random_file = open('reqs/random', 'r')
	read_file = random_file.read()
	return str(read_file)