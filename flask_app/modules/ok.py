from flask import Flask, render_template, url_for, request, redirect, session
from flask import Blueprint
from app.random_number import read_random

ok_bp = Blueprint('ok', __name__)

@ok_bp.route("/ok")
def ok():
        if 'email' in session:
                return render_template('ok.html', title='Личный VPN', req_id=read_random())
        return redirect(url_for('index.index'))