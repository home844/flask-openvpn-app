from app.create_vars import create_vars, copy_vars # own module
from app.write_req import create_dir, record_json # own module
from app.run_gen_req import run_easyrsa # own module
from app.random_number import random_number, read_random # own module
from app.forms import ReqForm # own module
from flask import Flask, render_template, url_for, request, redirect, session
from flask import Blueprint

index_bp = Blueprint('index', __name__)

@index_bp.route("/", methods=['GET', 'POST'])
def index():
	form = ReqForm()
	if form.validate_on_submit():
		if request.method == 'POST':
			random_number() # creating random file in reqs folder
			create_dir(read_random()) # creating directory with req_id number
			record_json(read_random()) # creating json in created directory
			create_vars(read_random()) # creating vars file in created directory
			copy_vars(read_random()) # coping vars file to pki folder
			run_easyrsa(read_random()) # generating request to pki
		session['email'] = request.form['email']
		return redirect(url_for('ok.ok'))
	return render_template('index.html', form=form, title='Личный VPN')