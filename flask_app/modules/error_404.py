from flask import Flask, render_template, url_for, request, redirect, session
from flask import Blueprint

error404_bp = Blueprint('404', __name__)

@error404_bp.app_errorhandler(404)
def page_not_found(error):
    return redirect('/')