# flask-openvpn-app
This application combines VPN server, PKI creation tool, web interface for ordering a certificate and reverse proxy.

Stack: OpenVPN, EasyRSA3, Flask + Gunicorn, Nginx, Telegram Bot (aiogram)

![alt text](schema.png)

## Launch Instruction
1) Copy repository and change the variables in easyrsa3/fast_deploy_CA.sh: 
```EASYRSA_REQ_COUNTRY
EASYRSA_REQ_PROVINCE
EASYRSA_REQ_CITY
EASYRSA_REQ_ORG
EASYRSA_REQ_EMAIL
EASYRSA_REQ_OU
EASYRSA_REQ_CN
```

2) Deploy PKI:  
```
cd easyrsa3 && fast_deploy_CA.sh
```

3) Change `SECRET_KEY` in flask_app/.env

4) Change `TOKEN` in tgbot/.env

5) Change `SERVER_HOSTNAME` in easyrsa3/create_cert.sh

6) Create docker network and run docker compose:  
```
docker network create flask_openvpn && docker compose up -d
```